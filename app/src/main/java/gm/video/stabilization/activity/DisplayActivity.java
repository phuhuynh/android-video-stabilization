package gm.video.stabilization.activity;

import androidx.annotation.Nullable;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import gm.video.stabilization.R;


public class DisplayActivity extends Activity {

    private String mImageURL = "sidewalk";
    ArrayList<Mat> src, src_gray;
    private int mFrameLength;

    private ImageView mRealImageView;
    private ImageView mStableImageView;
    private Button mRunButton;
    private Button mShowButton;

    private Timer mTimer;

    // OpenCV Loader.
    private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    //DO YOUR WORK/STUFF HERE
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        //init OpenCV
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mOpenCVCallBack);

        mRealImageView = (ImageView) findViewById(R.id.realFrame);
        mStableImageView = (ImageView) findViewById(R.id.stableFrame);
        mRunButton = (Button) findViewById(R.id.runButton);
        mShowButton = (Button) findViewById(R.id.showButton);

        AssetManager manager = getAssets();

        try {
            mFrameLength = Objects.requireNonNull(manager.list(mImageURL + "/")).length;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        mTimer = new Timer();
        src = new ArrayList<>(mFrameLength);
        src_gray = new ArrayList<>(mFrameLength);

        buttonListener();
    }

    private void buttonListener() {
        mRunButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Stabilization()) {
                    mShowButton.setEnabled(true);
                }
            }
        });

        mShowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageTask showImage = new ImageTask(mFrameLength);
                showImage(showImage);
            }
        });
    }

    private boolean Stabilization() {
        // TODO : Stabilization in HERE.
        // Change mTranformMatrix for stable image.
        AssetManager manager = getAssets();
        try {
            for (int i = 0; i < mFrameLength; i++) {
                final InputStream in;

                in = manager.open(mImageURL + "/frame_" + i + ".jpg");

                final Bitmap selectedImage = BitmapFactory.decodeStream(in);

                // Making THRESHOLD image.
                Mat t = new Mat(selectedImage.getHeight(), selectedImage.getWidth(), CvType.CV_8UC4);
                src.add(t);
                Utils.bitmapToMat(selectedImage, src.get(i));
                Mat g = new Mat(selectedImage.getHeight(), selectedImage.getWidth(), CvType.CV_8UC1);
                src_gray.add(g);
                Imgproc.cvtColor(src.get(i), src_gray.get(i), Imgproc.COLOR_BGR2GRAY);
                Imgproc.threshold(src_gray.get(i), src_gray.get(i), 100, 255, Imgproc.THRESH_BINARY);
                Imgproc.cvtColor(src_gray.get(i), src.get(i), Imgproc.COLOR_GRAY2RGBA, 4);
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


    private void showImage(ImageTask imageTask) {

        /**
         * timer delay : delay time before start task.
         * timer period : frame per second. ex : 50 ~ 1000/50 = 20 fps.
         * it just for display.
         */
        mTimer.scheduleAtFixedRate(imageTask, 10, 50);
    }

    private void DisplayImage(final ImageView view, final Bitmap img) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setImageBitmap(img);
            }
        });

    }

    class ImageTask extends TimerTask {
        private int length;
        private int i = 0;
        private boolean Done;

        public ImageTask(int len) {
            this.length = len;
            this.Done = false;
        }

        public void run() {
            // TimerTask for display sequence image.
            AssetManager manager = getAssets();
            try {
                if (i < length) {
                    final InputStream in = manager.open(mImageURL + "/frame_" + i + ".jpg");
                    final Bitmap selectedImage = BitmapFactory.decodeStream(in);

                    // Display Real Image
                    DisplayImage(mRealImageView, selectedImage);

                    // Display Stabilization Image
                    Bitmap processedImage = Bitmap.createBitmap(src.get(i).cols(), src.get(i).rows(), Bitmap.Config.ARGB_8888);
                    Log.i("imageType", CvType.typeToString(src.get(i).type()) + "");
                    Utils.matToBitmap(src.get(i), processedImage);
                    DisplayImage(mStableImageView, processedImage);
                    i++;
                }
                else{
                    this.Done = true;
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        public boolean isDone() {
            return Done;
        }
    }

}

